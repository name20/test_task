<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.22/css/jquery.dataTables.min.css"/>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/scroller/2.0.3/css/scroller.dataTables.min.css"/>
    <link href="{{ URL::asset('/app.css') }}" rel="stylesheet" type="text/css" >
    <link href="{{ asset('css/index.css') }}" rel="stylesheet" type="text/css" >
    <title>Document</title>
</head>
<body>
    <header>
        <nav>
            <div class='navItemWrap'>
                <a href="{{ route('orders.index') }}">Заказы</a>
            </div>
            <div class='navItemWrap'>
                <a href="{{ route('goods.index') }}">Товары</a>
            </div>
        </nav>
        <div class="hidden sm:flex sm:items-center sm:ml-6">
            <form method="POST" action="{{ route('logout') }}">
                @csrf
                <input class="submitBtn logOutBtn" type="submit" value="Log out">
            </form>
        </div>
    </header>
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.21/js/dataTables.bootstrap4.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/scroller/2.0.3/js/dataTables.scroller.min.js"></script>
    @yield('content')
    @yield('scripts')
</body>
</html>
