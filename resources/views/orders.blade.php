@extends('components.layout')
@section('content')
    <div class = 'ordersTableWrapper'>
        <h1>All orders</h1>
        <form class='ordersSearchForm' name='searchForm' data-url="{{ route('orders.getFilteredOrdersList') }}">
            <div>
                <label>
                    От
                </label>
                <br/>
                <input type="text" name="date_from">
            </div>
            <div>
                <label>
                    До
                </label>
                <br/>
                <input type="text" name="date_to">
            </div>
            <div>
                <label>
                    Статус
                </label>
                <br/>
                <select name = 'state'>
                    <option value=''>Все</option>
                    @foreach($statesArray as $key => $value)
                        <option value='{{ $key }}'>{{ $value }}</option>
                    @endforeach
                </select>
            </div>
            <div>
                <label>
                    Телефон
                </label>
                <br/>
                <input type="text" name="phone">
            </div>
            <div>
                <label>
                    Товар
                </label>
                <br/>
                <input type="text" name="good_name">
            </div>
            <div>
                <label>
                    Id заказа
                </label>
                <br/>
                <input type="text" name="order_id">
            </div>
            <div class="submitBtnWrap">
                <input type="submit" value='search' class='submitBtn'>
            </div>
        </form>
        <div>
            <table id="orders-table">
                <thead>
                <tr id = 'tableHeader' data-url="{{ route('orders.getFilteredOrdersList') }}">
                    <th data-column_number = 0>#</th>
                    <th data-column_number = 1>Дата</th>
                    <th data-column_number = 2>Клиент</th>
                    <th data-column_number = 3>Телефон</th>
                    <th data-column_number = 4>Товар</th>
                    <th data-column_number = 5>Статус</th>
                </tr>
                </thead>
            </table>
        </div>
    </div>
@endsection
@section('scripts')
    <script>
        $( document ).ready(function() {
            $("form[name='searchForm']").submit(function (e) {
                e.preventDefault();
                const table = $('#orders-table').DataTable();
                table.draw();
            });
        });

        const tableHeader = $('#tableHeader');
        let columnToSort = 0;

        tableHeader.on('click', 'thead', function(e) {
            columnToSort = e.target.dataset.column_number;
        });

        const table = $('#orders-table').DataTable({
            "processing": true,
            "serverSide": true,
            "order": [[ columnToSort, 'asc' ]],
            "ajax":{
                "url": $("form[name='searchForm']").attr('data-url'),
                "dataType": "json",
                "type": "POST",
                'data': function(data){

                    const state = $("select[name='state']").val();
                    const phone = $("input[name='phone']").val();
                    const good_name = $("input[name='good_name']").val();
                    const order_id = $("input[name='order_id']").val();
                    const date_from = $("input[name='date_from']").val();
                    const date_to = $("input[name='date_to']").val();

                    data._token = "{{csrf_token()}}";
                    data.state = state;
                    data.phone = phone;
                    data.good_name = good_name;
                    data.order_id = order_id;
                    data.date_from = date_from;
                    data.date_to = date_to;

                }
            },
            columns: [
                { "data": "id" },
                { "data": "date" },
                { "data": "user" },
                { "data": "phone" },
                { "data": "good" },
                { "data": "state" },
            ],
            searching: false,
        });
    </script>
@endsection
