@extends('components.layout')
@section('content')
    <div class="goodEditFormWrap">
        <form class = 'goodEditForm' method="POST" action = "{{ route('goods.update', $good->id) }}">
            @csrf
            <label>ID</label>
            <input type="text" name="id" value="{{ $good->id }}" readonly>
            <label>Рекламодатель</label>
            <select name="advert" class="advertsSelect">
                @foreach($advertsArray as $key => $advertFullName)
                    <option {{ $key == $good->advert->id ? 'selected' : null }} value="{{ $key }}">{{ $advertFullName }}</option>
                @endforeach
            </select>
            <label>Название</label>
            <input type="text" name="name" value="{{ $good->name }}">
            <label>Цена</label>
            <input type="text" name="price" value="{{ $good->price }}">
            <input type="submit" value="Save" class="submitBtn editBtn">
        </form>
    </div>
@endsection
