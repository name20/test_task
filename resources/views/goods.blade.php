@extends('components.layout')
@section('content')
    <div>
        <h1>Товары</h1>
    </div>
    <div class="goodsTableWrapper">
        <table id="goods-table" data-url="{{ route('goods.getGoodsList') }}">
            <thead>
            <tr id = 'tableHeader' >
                <th></th>
                <th data-column_number = 0>#</th>
                <th data-column_number = 1>Название</th>
                <th data-column_number = 2>Цена</th>
                <th data-column_number = 3>Рекламодатель</th>
            </tr>
            </thead>
        </table>
    </div>
@endsection
@section('scripts')
    <script>
        $( document ).ready(function() {
            const table = $('#goods-table').DataTable();
            table.draw();
        });

        const table = $('#goods-table').DataTable({
            "processing": true,
            "serverSide": true,
            "order": [[ 1, 'asc' ]],
            "ajax":{
                "url": $("#goods-table").attr('data-url'),
                "dataType": "json",
                "type": "POST",
                'data': function(data){
                    data._token = "{{csrf_token()}}";
                }
            },
            columns: [
                { "data": "edit" },
                { "data": "id" },
                { "data": "good_name" },
                { "data": "price" },
                { "data": "advert_name" },
            ],
            searching: false,
        });
    </script>
@endsection

