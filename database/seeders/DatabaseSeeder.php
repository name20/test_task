<?php

namespace Database\Seeders;

use App\Models\Good;
use App\Models\Order;
use App\Models\State;
use App\Models\User;
use App\Models\Advert;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    public $statesName = array(
        'Новый',
        'В работе',
        'Подтвержден'
    );

    public $statesSlug = array(
        'new',
        'onoperator',
        'accepted'
    );

    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $states = [];
        for ($i = 0; $i < 3; $i++) {
            $state = State::factory()->create([
                'name'=>$this->statesName[$i],
                'slug'=>$this->statesSlug[$i]
            ]);
            $states[] = $state;
        }

        $advertsIds = Advert::factory(5)->create()->pluck('id')->toArray();

        $goods = Good::factory(10)->make()
            ->each(function ($good) use($advertsIds){
                $good->advert_id = rand(1, count($advertsIds));
            });
        Good::query()->insert($goods->toArray());

        $goodsIds = $goods->pluck('id');

        User::factory(5)->create()->each(function($user) use ($goodsIds, $states){
            $user->orders()->saveMany(
                Order::factory()->count(3)->make()
                    ->each(function ($order) use($goodsIds, $states, $user){
                        $order->good_id = rand(1, count($goodsIds));
                        $order->state_id = rand(1, count($states));
                        $order->user_id = $user->id;
                    })
            );
        });
    }
}
//        $goods = [];
//        for($i = 0; $i > 4; $i++){
//            $good = Good::factory()->create([
//                'name' => $this->goods[$i],
//                'advert_id' => function() use ($advertCollection){
//                    return $advertCollection
//                        ->find(rand(0, count($advertCollection - 1)))->id;
//                }
//            ]);
//            $goods[] = $good;
//        }
