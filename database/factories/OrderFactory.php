<?php

namespace Database\Factories;

use App\Models\Advert;
use App\Models\Good;
use App\Models\Order;
use App\Models\State;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class OrderFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Order::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'state_id' => 1,
            'good_id' => 1,
            'user_id' => 1,
        ];
    }
}


//return [
//    'state_id' => function() {
//        return State::query()
//            ->find(rand(1, 3))->id;
//    },
//    'good_id' => function() {
//        return Good::query()
//            ->find(rand(0, 3))->id;
//    },
//    'user_id' => function() {
//        return User::query()
//            ->find(rand(1, User::all()->count()))->id;
//    },
//];
