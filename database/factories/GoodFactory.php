<?php

namespace Database\Factories;

use App\Models\Good;
use App\Models\Advert;
use Illuminate\Database\Eloquent\Factories\Factory;

$goods = array(
    'Часы Rado Integral',
    'Часы Swiss Army',
    'Детский планшет',
    'Колонки Monster Beats'
);

class GoodFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Good::class;

    public function randomPrice() {
        $price = $this->faker->randomDigit() * 20;
        if($price == 0) return GoodFactory::randomPrice();
        return $price;
    }

    /**
     * Define the model's default state.
     *
     * @return array
     */

    public function definition()
    {
        return [
            'name' => $this->faker->word(),
            'price' => $this::randomPrice(),
            'advert_id' => 1
        ];
    }
}
