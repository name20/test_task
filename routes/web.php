<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::middleware(['auth'])->group(function() {
    Route::get('/', 'OrderController@index')->name('orders.index');

    Route::post('/getFilteredOrdersList', 'OrderController@getFilteredOrdersList')
        ->name('orders.getFilteredOrdersList');

    Route::get('/goods', 'GoodController@index')->name('goods.index');

    Route::post('/goods', 'GoodController@getGoodsList')->name('goods.getGoodsList');

    Route::get('/goods/{good}', 'GoodController@getEdit')->name('goods.getEdit');

    Route::post('/goods/{good}', 'GoodController@update')->name('goods.update');
});

require __DIR__.'/auth.php';
