<?php

namespace App\Http\Controllers;

use App\Models\Order;
use App\Models\State;
use Carbon\Carbon;


class OrderController extends Controller
{
    public function index()
    {
        $statesArray = State::query()->pluck('name', 'id')->toArray();
        return view('orders', [
            'statesArray' => $statesArray
        ]);
    }

    public function getFilteredOrdersList()
    {
        $data = request()->all();

        $orders = Order::query()
            ->with('user', 'state', 'good');

        // filter by order state
        if (array_key_exists('state', $data) && !is_null($data['state'])) {
            $orders = $orders->where('state_id', $data['state']);
        }

        // filter by user phone
        if (array_key_exists('phone', $data) && !is_null($data['phone'])) {
            $orders = $orders->whereHas('user', function ($query) use ($data) {
                return $query->where('phone', 'like', '%'.$data['phone'].'%');
            });
        }

        // filter by good name
        if (array_key_exists('good_name', $data) && !is_null($data['good_name'])) {
            $orders = $orders->whereHas('good', function ($query) use ($data) {
                return $query->where('name', 'like', '%'.$data['good_name'].'%');
            });
        }

        // filter by order id
        if (array_key_exists('order_id', $data) && !is_null($data['order_id'])) {
            $orders = $orders->where('orders.id', $data['order_id']);
        }

        // filter by date
        if (array_key_exists('date_from', $data) && !is_null($data['date_from'])) {
            $dateFromInput = Carbon::parse($data['date_from'])->endOfDay();

            $orders = $orders->where('orders.created_at', '>=', $dateFromInput);
        }

        if (array_key_exists('date_to', $data) && !is_null($data['date_to'])) {
            $dateFromInput = Carbon::parse($data['date_to'])->endOfDay();

            $orders = $orders->where('orders.created_at', '<=', $dateFromInput);
        }

        return datatables()->of($orders
            ->orderBy('id')
            ->get())
            ->addColumn('id', function ($order) {
                return $order->id;
            })
            ->addColumn('date', function ($order) {
                return $order->created_at;
            })
            ->addColumn('user', function ($order) {
                return $order->user->full_name;
            })
            ->addColumn('phone', function ($order) {
                return $order->user->phone;
            })
            ->addColumn('good', function ($order) {
                return $order->good->name;
            })
            ->addColumn('state', function ($order) {
                return $order->state->name;
            })
            ->toJson();
    }
}
