<?php

namespace App\Http\Controllers;

use App\Models\Advert;
use App\Models\Good;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class GoodController extends Controller
{
    public function index(){
        return view('goods');
    }

    public function getGoodsList(){
        $goods = Good::query()->with('advert');

        return datatables()->of($goods
            ->orderBy('id')
            ->get())
            ->addColumn('edit', "<input type='checkbox'>")
            ->addColumn('id', function ($good) {
                return $good->id;
            })
            ->addColumn('good_name', function ($good) {
                return "<a href = '".route('goods.getEdit', $good->id)."'>$good->name</a>";
            })
            ->addColumn('price', function ($good) {
                return $good->price;
            })
            ->addColumn('advert_name', function ($good) {
                return $good->advert->first_name . ' ' . $good->advert->last_name;
            })
            ->rawColumns(['edit', 'good_name'])
            ->toJson();
    }

    public function getEdit(Good $good){
        $advertsArray = Advert::query()
            ->get()
            ->pluck("full_name", "id")
            ->toArray();

        return view('getEdit', [
            'advertsArray' => $advertsArray,
            'good' => $good
        ]);
    }

    public function update(Request $request, Good $good){
        $good->name = request('name');
        $good->price = request('price');
        $good->advert_id = request('advert');
        $good->save();

        return redirect()->route('goods.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Good  $good
     * @return \Illuminate\Http\Response
     */
    public function show(Good $good)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Good  $good
     * @return \Illuminate\Http\Response
     */


    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Good  $good
     * @return \Illuminate\Http\Response
     */
    public function destroy(Good $good)
    {
        //
    }
}
