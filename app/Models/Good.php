<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Good extends Model
{
    use HasFactory;

    public function advert() {
        return $this->belongsTo(Advert::class);
    }

    public function order() {
        return $this->belongsToMany(Order::class);
    }
}
